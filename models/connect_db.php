<?php
    // Réquisition du fichier env.php
    require('env.php');

    // Création d'une instance PDO dans la variable $PDOdb
    // On défini comme base de données "bddrVeille"
    // $user et $pass viennent du fichier env.php
    $PDOdb = new PDO('mysql:host=localhost;dbname=bddrVeille; charset=utf8', $user, $pass);