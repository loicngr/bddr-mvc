<?php
    require('controllers/functions.php');

    include('views/common/doctype.php');
    include('views/common/header.php');
    include('views/common/body_open.php');
    include('views/rubriques/main.php');
    include('views/common/body_close.php');
    include('views/common/html_close.php');