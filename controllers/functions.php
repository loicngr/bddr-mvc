<?php
    /**
     * Fonction qui récupére toutes les relations dans notre table bibliotheque
     */
    function getAllRelations() {
        // Importation du fichier de connexion dans notre base de données
        require('models/connect_db.php');

        // Préparation de la requête PDO avec INNER JOIN qui permet de joindre les deux tables bibliotheque et livres
        $requete = $PDOdb->prepare("SELECT (livres.titre), (livres.categorie), (bibliotheque.date_update) FROM bibliotheque RIGHT JOIN livres ON bibliotheque.id_livre = livres.id");
        // Execution de la requete PDO
        $requete->execute();
        // On retourne les valeurs en objets
        return $requete->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Fonction qui récupére un livre spécifique en fonction du titre dans la table bibliotheque et livres
     */
    function getRelations($book_name) {
        // Importation du fichier de connexion dans notre base de données
        require('models/connect_db.php');
        
        // Préparation de la requête PDO avec INNER JOIN qui permet de joindre les deux tables bibliotheque et livres et en cherchant avec le 
        // titre du livre dans la table livres
        $requete = $PDOdb->prepare("SELECT (livres.titre), (livres.categorie), (bibliotheque.date_update) FROM bibliotheque RIGHT JOIN livres ON bibliotheque.id_livre = livres.id WHERE livres.titre = (:book)");
        // On lie à notre paramètre :book notre variable $book_name
        $requete->bindParam(':book', $book_name);
        // Execution de la requete PDO
        $requete->execute();
        // On retourne la valeur en objet
        return $requete->fetch(PDO::FETCH_OBJ);
    }